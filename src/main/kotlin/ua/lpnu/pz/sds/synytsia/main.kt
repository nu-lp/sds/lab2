package ua.lpnu.pz.sds.synytsia

import tornadofx.launch
import ua.lpnu.pz.sds.synytsia.core.Md5
import ua.lpnu.pz.sds.synytsia.extensions.reverseBytes
import java.io.ByteArrayInputStream

@ExperimentalStdlibApi
@ExperimentalUnsignedTypes
fun main() {
    launch<Application>()
//    val md5 = Md5()
//    val string = "sThe quick brown fox jumps over the lazy dog. flad;sjflaj d;lfja  fldjsa jfasl;dk jfas jdlfksa;d hflkjasd lkhaf sdkjfh alshfd kaljs dhf"
//    // d41d8cd98f00b204e9800998ecf8427e
//    val stream = ByteArrayInputStream(string.toByteArray(Charsets.UTF_8))
//    println(md5.calc(stream))
//    println()
//    val num = 0xabcdef12u
//    println(num.toString(16))
//    println( num.reverseBytes().toString(16))
//    val md5 = Md5()
//    val testArr = UIntArray(16)
//    testArr[0] = 2150982765u
//    testArr[14] = 24u
//    md5.process(testArr)
//    println(md5.calcHash())
}