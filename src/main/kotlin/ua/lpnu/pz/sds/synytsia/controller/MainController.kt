package ua.lpnu.pz.sds.synytsia.controller

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.Controller
import ua.lpnu.pz.sds.synytsia.core.Md5
import java.io.BufferedWriter
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileWriter

@ExperimentalStdlibApi
@ExperimentalUnsignedTypes
class MainController() : Controller() {
    private val md5 = Md5()
    val filePath = SimpleObjectProperty<File>()
    val inputString = SimpleStringProperty("")
    val hash = SimpleStringProperty()
    val expectedHash = SimpleStringProperty("")
    val resultComparison = SimpleStringProperty()

    fun hashFile() {
        hash.value = "Processing..."
        if (filePath.value == null || !filePath.value.exists()) {
            hash.value = "File does not exists"
            return
        }
        runAsync {
            md5.calc(filePath.value.inputStream())
        } ui {
            hash.value = it
        }
    }

    fun hashString() {
        hash.value = "Processing..."
        runAsync {
            md5.calc(ByteArrayInputStream(inputString.value.toByteArray()))
        } ui {
            hash.value = it
        }
    }

    fun compareFile() {
        resultComparison.value = "Processing..."
        if (filePath.value == null || !filePath.value.exists()) {
            resultComparison.value = "File does not exists"
            return
        }
        runAsync {
            md5.calc(filePath.value.inputStream())
        } ui {
            println(it)
            println(expectedHash.value)
            resultComparison.value = when (it.toUpperCase() == expectedHash.value.toUpperCase()) {
                true -> "Hash matches"
                false -> "Hash does not match"
            }
        }
    }

    fun compareString() {
        resultComparison.value = "Processing..."
        runAsync {
            md5.calc(ByteArrayInputStream(inputString.value.toByteArray()))
        } ui {
            println(it)
            println(expectedHash.value)
            resultComparison.value = when (it.toUpperCase() == expectedHash.value.toUpperCase()) {
                true -> "Hash matches"
                false -> "Hash does not match"
            }
        }
    }

    fun exportHash(filePath: String) {
        BufferedWriter(FileWriter(filePath)).use {
            it.write(hash.value)
        }
    }
}