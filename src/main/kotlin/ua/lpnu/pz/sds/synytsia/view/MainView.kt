package ua.lpnu.pz.sds.synytsia.view

import javafx.scene.layout.Priority
import tornadofx.*
import ua.lpnu.pz.sds.synytsia.controller.MainController

@ExperimentalStdlibApi
@ExperimentalUnsignedTypes
class MainView : View("Lab2 - MD5") {
    private val controller: MainController by inject()


    override val root = vbox {
        minWidth = 640.0
        minHeight = 480.0
        form {
            fieldset("Input") {
                field("Input file") {
                    hbox {
                        label(controller.filePath) {
                            hgrow = Priority.ALWAYS
                            useMaxSize = true
                        }
                        button("Browse...") {
                            hboxConstraints {
                                marginLeft = 16.0
                            }
                            action {
                                val dir = chooseFile("Select input file", emptyArray())
                                if (dir.isNotEmpty()) {
                                    controller.filePath.set(dir[0])
                                }
                            }
                        }
                        button("Hash") {
                            hboxConstraints {
                                marginLeft = 16.0
                            }
                            action {
                                controller.hashFile()
                            }
                        }
                        button("Compare") {
                            hboxConstraints {
                                marginLeft = 16.0
                            }
                            action {
                                controller.compareFile()
                            }
                        }

                    }
                }
                vbox {
                    field("Input string") {
                        button("Hash") {
                            hboxConstraints {
                                marginLeft = 16.0
                            }
                            action {
                                controller.hashString()
                            }
                        }
                        button("Compare") {
                            hboxConstraints {
                                marginLeft = 16.0
                            }
                            action {
                                controller.compareString()
                            }
                        }

                    }
                    textarea(controller.inputString) {
                    }
                }
                field("Expected hash") {
                    hbox {
                        textfield(controller.expectedHash) {

                        }
                        label(controller.resultComparison)
                    }
                }
            }
            fieldset("Hash") {
                textfield(controller.hash) {
                    isEditable = false
                }

                button("Export") {

                    vboxConstraints {
                        marginTop = 16.0
                    }
                    action {
                        val dir = chooseDirectory("Select target directory")
                        dir?.let {
                            controller.exportHash("$dir\\$FILENAME")
                        }
                    }
                }

            }
        }
    }

    companion object {
        const val FILENAME = "Output.txt"
    }
}
