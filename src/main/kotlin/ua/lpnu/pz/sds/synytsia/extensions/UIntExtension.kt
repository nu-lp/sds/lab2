package ua.lpnu.pz.sds.synytsia.extensions

@ExperimentalUnsignedTypes
fun UInt.reverseBytes(): UInt {
    return ((this and 0xffu) shl 24) or
            ((this and 0xff00u) shl 8) or
            ((this and 0xff0000u) shr 8) or
            ((this and 0xff000000u) shr 24)
}
