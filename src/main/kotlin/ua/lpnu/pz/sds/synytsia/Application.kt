package ua.lpnu.pz.sds.synytsia

import tornadofx.App
import ua.lpnu.pz.sds.synytsia.view.MainView

@ExperimentalUnsignedTypes
@ExperimentalStdlibApi
class Application : App(MainView::class, Styles::class)