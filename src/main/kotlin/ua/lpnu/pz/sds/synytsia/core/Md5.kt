package ua.lpnu.pz.sds.synytsia.core

import ua.lpnu.pz.sds.synytsia.extensions.reverseBytes
import java.io.InputStream
import kotlin.math.abs
import kotlin.math.sin

@ExperimentalStdlibApi
@ExperimentalUnsignedTypes
class Md5(
    /**
     * Minimum - 128 bytes
     */
    bufferSize: Int = 1_048_576
) {
    private var A0: UInt = 0x67452301u
    private var B0: UInt = 0xefcdab89u
    private var C0: UInt = 0x98badcfeu
    private var D0: UInt = 0x10325476u
    private val buffer = ByteArray(bufferSize.coerceAtLeast(128))

    /**
     * Function per round
     */
    private val funcs = arrayOf(this::F, this::G, this::H, this::I)

    /**
     * Chunk index per round
     */
    private val chunkIdx = arrayOf<(Int) -> Int>(
        { it },
        { (it * 5 + 1) % 16 },
        { (it * 3 + 5) % 16 },
        { (it * 7) % 16 })

    private val T = UIntArray(64) { (4294967296 * abs(sin(it + 1.0))).toUInt() }
    private val S = intArrayOf(
        7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
        5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
        4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
        6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
    )
    private val padding = byteArrayOf(
        0x80.toByte(), 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    )

    private fun reset() {
        A0 = 0x67452301u
        B0 = 0xefcdab89u
        C0 = 0x98badcfeu
        D0 = 0x10325476u
    }

    fun calc(input: InputStream): String {
        reset()
        var bitsRead = 0L
        var bytesRead: Int
        var bytesLeft = 0
        var startIdx = 0
        input.use {
            while (it.read(buffer).also { bytesRead = it } != -1) {
                bitsRead += bytesRead * 8L
                if (bytesRead < buffer.size) {
                    bytesLeft = bytesRead % 64
                    bytesRead -= bytesLeft
                    startIdx = bytesRead
                }
                processBuffer(buffer, bytesRead)
            }
        }
        buffer.copyInto(buffer, 0, startIdx, startIdx + bytesLeft)
        val neededPadding = (120 - (bytesLeft + 1)) % 64 + 1
        padding.copyInto(buffer, bytesLeft, 0, neededPadding)
        bytesRead = bytesLeft + neededPadding
        for (i in 0..7) {
            buffer[bytesRead + i] = (bitsRead ushr (8 * i)).toByte()
        }
        bytesRead += 8
        processBuffer(buffer, bytesRead)

        return calcHash()
    }

    private fun processBuffer(buffer: ByteArray, bufSize: Int) {
        val uBuffer = buffer.toUByteArray()
        for (chunkIdx in 0 until bufSize step 64) {
            val block = UIntArray(16)
            for (i in 0..15) {
                val b1 = uBuffer[chunkIdx + i * 4]
                val b2 = uBuffer[chunkIdx + i * 4 + 1]
                val b3 = uBuffer[chunkIdx + i * 4 + 2]
                val b4 = uBuffer[chunkIdx + i * 4 + 3]
                block[i] = bytesToLowOrderWord(b1, b2, b3, b4)
            }
            process(block)
        }
    }

    /**
     * Example: b1b2b3b4 becomes word in order b4b3b2b1
     */
    private fun bytesToLowOrderWord(b1: UByte, b2: UByte, b3: UByte, b4: UByte): UInt {
        return (b4.toUInt() shl 0x18) + (b3.toUInt() shl 0x10) + (b2.toUInt() shl 0x08) + (b1.toUInt())
    }

    private fun process(chunk: UIntArray) {
        var A = A0
        var B = B0
        var C = C0
        var D = D0

        for (i in 0..3) {
            val f = funcs[i]
            val idx = chunkIdx[i]
            for (j in 0..15) {
                val F = A + f(B, C, D) + chunk[idx(j)] + T[i * 16 + j]
                A = D
                D = C
                C = B
                B = B + F.rotateLeft(S[i * 16 + j])
            }
        }
        A0 += A
        B0 += B
        C0 += C
        D0 += D
    }

    private fun calcHash() = "${A0.reversedHex()}${B0.reversedHex()}${C0.reversedHex()}${D0.reversedHex()}"

    private fun UInt.reversedHex() = this.reverseBytes().toString(16).let {
        when (it.length) {
            8 -> it
            else -> "0$it"
        }
    }

    private fun F(X: UInt, Y: UInt, Z: UInt): UInt = (X and Y) or (X.inv() and Z)

    private fun G(X: UInt, Y: UInt, Z: UInt): UInt = (X and Z) or (Y and Z.inv())

    private fun H(X: UInt, Y: UInt, Z: UInt): UInt = X xor Y xor Z

    private fun I(X: UInt, Y: UInt, Z: UInt): UInt = Y xor (X or Z.inv())
}