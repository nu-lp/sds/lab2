package ua.lpnu.pz.sds.synytsia.core

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@ExperimentalStdlibApi
class Md5Test {
    //536_870_912
    private val md5 = Md5(128)

    @Test
    fun hashTest1() {
        val testString = ""
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("d41d8cd98f00b204e9800998ecf8427e", hash)
    }

    @Test
    fun hashTest2() {
        val testString = "a"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("0cc175b9c0f1b6a831c399e269772661", hash)
    }

    @Test
    fun hashTest3() {
        val testString = "abc"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("900150983cd24fb0d6963f7d28e17f72", hash)
    }

    @Test
    fun hashTest4() {
        val testString = "message digest"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("f96b697d7cb7938d525a2f31aaf161d0", hash)
    }

    @Test
    fun hashTest5() {
        val testString = "abcdefghijklmnopqrstuvwxyz"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("c3fcd3d76192e4007dfb496cca67e13b", hash)
    }

    @Test
    fun hashTest6() {
        val testString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("d174ab98d277d9f5a5611c2c9f419d9f", hash)
    }

    @Test
    fun hashTest7() {
        val testString = "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("57edf4a22be3c955ac49da2e2107b67a", hash)
    }

    @Test
    fun hashTest8() {
        val testString =
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("268c7919189d85e276d74b8c60b2f84f", hash)
    }

    @Test
    fun hashTest9() {
        val testString =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("c0c14598088798dcd13b43aa4f97e297", hash)
    }


    @Test
    fun hashTest10() {
        val testString =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789123"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("e2e2daff6469fa352e419023b932a083", hash)
    }

    @Test
    fun hashTest11() {
        val testString =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567891234"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("30ddb5dd86019d68be09e1f94c9fec53", hash)
    }

    @Test
    fun hashTest12() {
        val testString =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678912345"
        val hash = md5.calc(testString.byteInputStream())
        assertEquals("c8d91260801f68890e8b1572a109c21d", hash)
    }
}